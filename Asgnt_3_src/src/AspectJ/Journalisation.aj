package AspectJ;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import edu.uqac.aop.chess.agent.Move;

public aspect Journalisation {
	File file;
	boolean save = false;

	pointcut play() 
	: execution(* edu.uqac.aop.chess.Chess.play());

	before() : play() {
		JOptionPane jop = new JOptionPane();
		int retour = JOptionPane.showConfirmDialog(jop, "Voulez-vous enregistrer la partie ?",
				"Enregistrement de la partie", JOptionPane.YES_NO_OPTION);
		if (retour == jop.YES_OPTION) {
			save = true;
			JFileChooser jfc = new JFileChooser("./src/Parties enregistr�es");
			jfc.setDialogTitle("Selectionnez un fichier.txt ou enregistrer le fichier");
			String suffixe = "";
			String path;
			do {
				int ret = jfc.showSaveDialog(jfc);
				if (ret == JFileChooser.APPROVE_OPTION) {
					path = jfc.getSelectedFile().getAbsolutePath();
					if (path.contains(".")) {
						int i = path.lastIndexOf('.');
						if (i > 0 && i < path.length() - 1) {
							suffixe = path.substring(i + 1).toLowerCase();
						}
					}
					if (!suffixe.equals("txt")) {
						JOptionPane.showMessageDialog(null, "L'extension du fichier doit �tre .txt");
					}
				} else {
					path = "./src/Parties enregistr�es/Parties enregistr�es.txt";
					suffixe = "txt";
				}
			} while (!suffixe.equals("txt"));
			file = new File(path);
			try {
				FileWriter fw = new FileWriter(file, true);
				fw.write("------------------------------" + System.lineSeparator() + "Partie du "
						+ (java.time.LocalDateTime.now().getDayOfMonth() < 10 ? "0" : "")
						+ java.time.LocalDateTime.now().getDayOfMonth() + "/"
						+ (java.time.LocalDateTime.now().getMonthValue() < 10 ? "0" : "")
						+ java.time.LocalDateTime.now().getMonthValue() + "/" + java.time.LocalDateTime.now().getYear()
						+ " � " + (java.time.LocalDateTime.now().getHour() < 10 ? "0" : "")
						+ java.time.LocalDateTime.now().getHour() + ":"
						+ (java.time.LocalDateTime.now().getMinute() < 10 ? "0" : "")
						+ java.time.LocalDateTime.now().getMinute() + " : " + System.lineSeparator());
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			save = false;
		}
	}

	pointcut movePiece() 
	: execution(* edu.uqac.aop.chess.Board.movePiece(..));

	after() : movePiece() {
		if (save) {
			Move mv = new Move(0, 0, 0, 0);
			for (Object arg : thisJoinPoint.getArgs()) {
				if (arg instanceof Move) {
					mv = (Move) arg;
				}
			}
			String str = mv.toString();
			String ret = str.substring(0, 1).toUpperCase();
			ret += Integer.parseInt(str.substring(1, 2)) + 1;
			ret += " -> ";
			ret += str.substring(2, 3).toUpperCase();
			ret += Integer.parseInt(str.substring(3, 4)) + 1;
			ret += System.lineSeparator();
			try {
				FileWriter fw = new FileWriter(file, true);
				fw.write(ret);
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}