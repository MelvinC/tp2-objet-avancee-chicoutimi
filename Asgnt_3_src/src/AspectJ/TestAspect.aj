package AspectJ;

import edu.uqac.aop.chess.agent.Player;
import edu.uqac.aop.chess.agent.Move;

public aspect TestAspect {

	pointcut verifCoup(Player p, Move mv) : execution(boolean Player.makeMove(Move)) && this(p) && args(mv);

	boolean around(Player p, Move mv) : verifCoup(p, mv){
		if (mv.xF > 7 || mv.yF > 7 || mv.xF < 0 || mv.yF < 0) {
			if (p.getColor() == Player.BLACK)
				System.out.println("Vous ne pouvez pas vous d�placer � l'ext�rieur du plateau !");
			p.makeMove();
			return true;
		}
		if (p.getPlayground().getGrid()[mv.xF][mv.yF].isOccupied()
				&& p.getPlayground().getGrid()[mv.xF][mv.yF].getPiece().getPlayer() != p.getColor()) {
			if (p.getColor() == Player.BLACK)
				System.out.println("Vous ne pouvez pas manger vos propres pi�ces !");
			p.makeMove();
			return true;
		}
		if (p.getPlayground().getGrid()[mv.xI][mv.yI].getPiece() != null) {
			if (p.getPlayground().getGrid()[mv.xI][mv.yI].getPiece().getPlayer() == p.getColor()) {
				if (p.getColor() == Player.BLACK)
					System.out.println("Vous ne pouvez pas jouer une pi�ce adverse !");
				p.makeMove();
				return true;
			}
			if (!p.getPlayground().getGrid()[mv.xI][mv.yI].getPiece().isMoveLegal(mv)) {
				if (p.getColor() == Player.BLACK)
					System.out.println("Vous n'avez pas le droit de vous d�placer ainsi !");
				p.makeMove();
				return true;
			}
			switch (p.getPlayground().getGrid()[mv.xI][mv.yI].getPiece().toString()) {
			case "P":
				if (mv.yF < mv.yI) {
					if (p.getColor() == Player.BLACK)
						System.out.println("Vous ne pouvez pas vous d�placer en arri�re!");
					p.makeMove();
					return true;
				}
				if (mv.yI != 1 && mv.yF - mv.yI > 1) {
					if (p.getColor() == Player.BLACK)
						System.out.println("Vous ne pouvez pas vous d�placer de 2 apr�s le premier coup!");
					p.makeMove();
					return true;
				}
				if (mv.yF - mv.yI == 1 && Math.abs(mv.xF - mv.xI) == 1
						&& p.getPlayground().getGrid()[mv.xF][mv.yF].getPiece() == null) {
					if (p.getColor() == Player.BLACK)
						System.out.println(
								"Vous ne pouvez pas vous d�placer en diagonale si ce n'est pas pour manger une pi�ce adverse!");
					p.makeMove();
					return true;
				}
				if (mv.xF - mv.xI == 0 && mv.yF - mv.yI == 1 && p.getPlayground().getGrid()[mv.xF][mv.yF].isOccupied()) {
					if (p.getColor() == Player.BLACK)
						System.out.println("Vous ne pouvez pas manger ainsi une pi�ce de votre adversaire !");
					p.makeMove();
					return true;
				}

				break;
			case "p":
				if (mv.yF > mv.yI) {
					if (p.getColor() == Player.BLACK)
						System.out.println("Vous ne pouvez pas vous d�placer en arri�re!");
					p.makeMove();
					return true;
				}
				if (mv.yI != 6 && mv.yI - mv.yF > 1) {
					if (p.getColor() == Player.BLACK)
						System.out.println("Vous ne pouvez pas d�placer de 2 apr�s le premier coup!");
					p.makeMove();
					return true;
				}
				if (mv.yI - mv.yF == 1 && Math.abs(mv.xF - mv.xI) == 1
						&& (p.getPlayground().getGrid()[mv.xF][mv.yF].getPiece() == null
								|| p.getPlayground().getGrid()[mv.xF][mv.yF].getPiece().getPlayer() == p.getColor())) {
					if (p.getColor() == Player.BLACK)
						System.out.println(
								"Vous ne pouvez pas vous d�placer en diagonale si ce n'est pas pour manger une pi�ce adverse!");
					p.makeMove();
					return true;
				}
				if (mv.xF - mv.xI == 0 && mv.yI - mv.yF == 1 && p.getPlayground().getGrid()[mv.xF][mv.yF].isOccupied()
						&& p.getPlayground().getGrid()[mv.xF][mv.yF].getPiece().getPlayer() == p.getColor()) {
					if (p.getColor() == Player.BLACK)
						System.out.println("Vous ne pouvez pas manger ainsi une pi�ce de votre adversaire !");
					p.makeMove();
					return true;
				}
				break;
			default:
				break;
			}
		}

		int colision = 0;
		if (p.getPlayground().getGrid()[mv.xI][mv.yI].getPiece() != null) {
			switch (p.getPlayground().getGrid()[mv.xI][mv.yI].getPiece().toString().toLowerCase()) {
			case "t":
				if (mv.xF - mv.xI == 0) {
					if (mv.yI < mv.yF) {
						for (int i = mv.yI + 1; i < mv.yF; i++)
							if (p.getPlayground().getGrid()[mv.xI][i].isOccupied())
								colision++;
					} else
						for (int i = mv.yI - 1; i > mv.yF; i--)
							if (p.getPlayground().getGrid()[mv.xI][i].isOccupied())
								colision++;
				} else {
					if (mv.xI < mv.xF) {
						for (int i = mv.xI + 1; i < mv.xF; i++)
							if (p.getPlayground().getGrid()[i][mv.yI].isOccupied())
								colision++;
					} else
						for (int i = mv.xI - 1; i > mv.xF; i--)
							if (p.getPlayground().getGrid()[i][mv.yI].isOccupied())
								colision++;
				}
				break;
			case "f":
				if (mv.xI < mv.xF) {
					if (mv.yI < mv.yF) {
						for (int i = 1; i < mv.xF - mv.xI; i++)
							if (p.getPlayground().getGrid()[mv.xI + i][mv.yI + i].isOccupied())
								colision++;
					} else
						for (int i = 1; i < mv.xF - mv.xI; i++)
							if (p.getPlayground().getGrid()[mv.xI + i][mv.yI - i].isOccupied())
								colision++;
				} else if (mv.yI < mv.yF) {
					for (int i = 1; i < mv.xI - mv.xF; i++)
						if (p.getPlayground().getGrid()[mv.xI - i][mv.yI + i].isOccupied())
							colision++;
				} else
					for (int i = 1; i < mv.xI - mv.xF; i++)
						if (p.getPlayground().getGrid()[mv.xI - i][mv.yI - i].isOccupied())
							colision++;
				break;
			case "d":
				if (mv.xF - mv.xI == 0 || mv.yF - mv.yI == 0)
					if (mv.xF - mv.xI == 0) {
						if (mv.yI < mv.yF) {
							for (int i = mv.yI + 1; i < mv.yF; i++)
								if (p.getPlayground().getGrid()[mv.xI][i].isOccupied())
									colision++;
						} else
							for (int i = mv.yI - 1; i > mv.yF; i--)
								if (p.getPlayground().getGrid()[mv.xI][i].isOccupied())
									colision++;
					} else {
						if (mv.xI < mv.xF) {
							for (int i = mv.xI + 1; i < mv.xF; i++)
								if (p.getPlayground().getGrid()[i][mv.yI].isOccupied())
									colision++;
						} else
							for (int i = mv.xI - 1; i > mv.xF; i--)
								if (p.getPlayground().getGrid()[i][mv.yI].isOccupied())
									colision++;
					}
				else {
					if (mv.xI < mv.xF) {
						if (mv.yI < mv.yF) {
							for (int i = 1; i < mv.xF - mv.xI; i++)
								if (p.getPlayground().getGrid()[mv.xI + i][mv.yI + i].isOccupied())
									colision++;
						} else
							for (int i = 1; i < mv.xF - mv.xI; i++)
								if (p.getPlayground().getGrid()[mv.xI + i][mv.yI - i].isOccupied())
									colision++;
					} else if (mv.yI < mv.yF) {
						for (int i = 1; i < mv.xI - mv.xF; i++)
							if (p.getPlayground().getGrid()[mv.xI - i][mv.yI + i].isOccupied())
								colision++;
					} else
						for (int i = 1; i < mv.xI - mv.xF; i++)
							if (p.getPlayground().getGrid()[mv.xI - i][mv.yI - i].isOccupied())
								colision++;
				}
				break;
			case "p":
				if (mv.yI < mv.yF) {
					for (int i = mv.yI + 1; i < mv.yF; i++)
						if (p.getPlayground().getGrid()[mv.xI][i].isOccupied())
							colision++;
				} else
					for (int i = mv.yI - 1; i > mv.yF; i--)
						if (p.getPlayground().getGrid()[mv.xI][i].isOccupied())
							colision++;
				break;
			default:
				break;
			}
			if (colision > 0) {
				if (p.getColor() == Player.BLACK)
					System.out.println("Vous ne pouvez pas passer par dessus des pi�ces !");
				p.makeMove();
				return true;
			}
		}
		return proceed(p, mv);
	}
}
