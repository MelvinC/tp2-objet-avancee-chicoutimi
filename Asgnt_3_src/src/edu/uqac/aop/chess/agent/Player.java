package edu.uqac.aop.chess.agent;

import edu.uqac.aop.chess.Board;

public abstract class Player {
	public static final int WHITE = 1;
	public static final int BLACK = 0;
	
	protected int Color;
	protected Board playGround;

	public abstract boolean makeMove(Move mv);
	public abstract Move makeMove();
	
	public int getColor(){
		return this.Color;
	}
	public void setColor(int arg){
		this.Color = arg;
	}
	
	public Board getPlayground() {
		return this.playGround;
	}
}
