package UI;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import components.Element;
import components.Food;
import game.Game;

public class Button_Square extends JButton implements MouseListener {

	private Game game;
	private int i;
	private int j; 
	

	public Button_Square(Game game, int i, int j) {
		this.game=game;
		this.i=i;
		this.j=j;
		this.addMouseListener(this);
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		Food food =new Food(i, j);
		game.getBoard().get(i, j).add(food);
		game.addFood(food);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {

	}

	@Override
	public void mouseExited(MouseEvent arg0) {

	}

	@Override
	public void mousePressed(MouseEvent arg0) {

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {

	}

}
