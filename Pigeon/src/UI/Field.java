package UI;

import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import components.Food;
import components.Pigeon;
import game.Game;

public class Field extends JPanel {

	public static final int X = 20;
	public static final int Y = 20;
	public static final int WIDTH = 1460;
	public static final int HEIGHT = 960;

	private Game game;
	private Button_Square[][] buttons;

	public Field(Game game) {
		super();
		this.game = game;
		this.buttons = new Button_Square[this.game.getBoard().LENGTH][this.game.getBoard().HEIGHT];
        this.setLayout(null);
		for (int i = 0; i < this.game.getBoard().LENGTH; i++)
			for (int j = 0; j < this.game.getBoard().HEIGHT; j++) {
				this.buttons[i][j] = new Button_Square(game, i, j);
				this.buttons[i][j].setBounds(X+i*(WIDTH / this.game.getBoard().LENGTH), Y+j*(HEIGHT / this.game.getBoard().HEIGHT), (WIDTH / this.game.getBoard().LENGTH), (HEIGHT / this.game.getBoard().HEIGHT));
				this.buttons[i][j].setOpaque(false);
				this.buttons[i][j].setContentAreaFilled(false);
				this.buttons[i][j].setBorderPainted(false);
				this.add(this.buttons[i][j]);
			}
	}

	public void paintComponent(Graphics g) {
		drawField(g);
		drawPigeon(g);
		drawFood(g);
	}

	public void drawField(Graphics g) {
		Image image;
		try {
			image = ImageIO.read(getClass().getResource("pave.png"));
			g.drawImage(image, X, Y, WIDTH, HEIGHT, null);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void drawPigeon(Graphics g) {
		for (Pigeon p : game.getPigeons())
			g.drawImage(p.getImage(), X + p.getX() * (WIDTH / this.game.getBoard().LENGTH),
					Y + p.getY() * (HEIGHT / this.game.getBoard().HEIGHT), 1024 / 10, 1024 / 10, null);
	}

	public void drawFood(Graphics g) {
		for (Food f : game.getFoods())
			g.drawImage(f.getImage(), X + f.getX() * (WIDTH / this.game.getBoard().LENGTH),
					Y + f.getY() * (HEIGHT / this.game.getBoard().HEIGHT), 256 / 5, 256 / 5, null);
	}
	
	public void drawPerturb(Graphics g, Image perturb) {
		g.drawImage(perturb, X, Y, WIDTH, HEIGHT, null);
	}
}
