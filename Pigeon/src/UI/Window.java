package UI;

import java.awt.Image;

import javax.swing.JFrame;

import game.Game;

public class Window extends JFrame {

	private Field field;
	private Game game;
	
	public Window(Game game) {
		super();
		this.game = game;
		this.field = new Field(game);
		
		this.setTitle("Pigeon");
	    this.setSize(1500, 1000);
	    this.setLocationRelativeTo(null);               
	    this.setContentPane(field);               
	    this.setVisible(true);
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void drawPerturb(Image perturb) {
		this.field.drawPerturb(this.getGraphics(), perturb);
	}
}
