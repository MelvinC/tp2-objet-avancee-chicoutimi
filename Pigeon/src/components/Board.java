package components;

public class Board {

	public static final int LENGTH = 30;
	public static final int HEIGHT = 30;

	private Element[][] matrix;

	public Board() {
		this.matrix = new Element[LENGTH][HEIGHT];
		for (int i = 0; i < LENGTH; i++) {
			for (int j = 0; j < HEIGHT; j++) {
				this.matrix[i][j] = new Element();
			}
		}
	}
	
	public void add(int x, int y, Prototype prototype) {
		this.matrix[x][y].add(prototype);
	}

	public Element get(int x, int y) {
		return this.matrix[x][y];
	}
	
	public Element[][] getMatrix(){
		return this.matrix;
	}
	
	public void removeProtoype(int x, int y, Prototype prototype) {
		this.matrix[x][y].prots.remove(prototype);
	}

}
