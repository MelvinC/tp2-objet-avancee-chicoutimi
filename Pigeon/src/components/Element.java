package components;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class Element {

	protected List<Prototype> prots;

	public Element() {
		this.prots = new CopyOnWriteArrayList<Prototype>();
	}

	public List<Prototype> getList() {
		return prots;
	}

	public void add(Prototype p) {
		this.prots.add(p);
	}
}
