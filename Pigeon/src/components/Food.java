package components;

import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;

public class Food extends Prototype {

	public static final int LIFE_MAX = 90;

	private int life_counter;

	public Food(int x, int y) {
		try {
			this.img = ImageIO.read(getClass().getResource("../UI/Food_30.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.life_counter = LIFE_MAX;
		this.coord = new Vector<Integer>();
		this.coord.add(x);
		this.coord.add(y);
	}

	public int getLife() {
		return this.life_counter;
	}

	public void decrLife() {
		this.life_counter--;
		if(life_counter > 60)
			try {
				this.img=ImageIO.read(getClass().getResource("../UI/Food_30.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		else if(life_counter > 30)
			try {
				this.img=ImageIO.read(getClass().getResource("../UI/Food_20.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		else if(life_counter > 0)
			try {
				this.img=ImageIO.read(getClass().getResource("../UI/Food_10.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		else
			try {
				this.img=ImageIO.read(getClass().getResource("../UI/Food_0.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
}
