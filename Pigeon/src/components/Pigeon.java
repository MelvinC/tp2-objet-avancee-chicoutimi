package components;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.imageio.ImageIO;

public class Pigeon extends Prototype implements Runnable {

	public enum Pigeon_State {
		ASLEPP, AWAKE
	}

	private Pigeon_State status;
	private Board board;

	public Pigeon(int x, int y, Board board) {
		try {
			this.img = ImageIO.read(getClass().getResource("../UI/Pigeon_Awake.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.status = Pigeon_State.AWAKE;
		
		this.coord = new Vector<Integer>();
		this.coord.add(x);
		this.coord.add(y);
		this.board = board;
	}

	// Pigeon's behavior into the thread
	@Override
	public void run() {
		while (true) {
			
			List<Food> foods = getFood();
			if (!foods.isEmpty()) {
				this.status = Pigeon_State.AWAKE;
				try {
					this.img = ImageIO.read(getClass().getResource("../UI/Pigeon_Awake.png"));
				} catch (IOException e) {
					e.printStackTrace();
				}
				List<Food> freshest = getFreshestFood(foods);
				Food closest = getClosestFood(freshest);
				this.move(closest);
			} else
				this.status = Pigeon_State.ASLEPP;
			try {
				this.img = ImageIO.read(getClass().getResource("../UI/Pigeon_Asleep.png"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void move(Food closest) {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if ((closest.getX() == this.getX() && closest.getY() == this.getY()))
			return;
		else if (closest.getX() == this.getX())
			this.moveY(closest);
		else if (closest.getY() == this.getY())
			this.moveX(closest);
		else {
			double temp = Math.random();
			if (temp > 0.5)
				this.moveX(closest);
			else
				this.moveY(closest);
		}
		this.run();
	}

	private void moveX(Food closest) {
		this.board.removeProtoype(this.getX(), this.getY(), this);
		if (this.getX() < closest.getX())
			this.setX(this.getX() + 1);
		else
			this.setX(this.getX() - 1);
		this.board.add(this.getX(), this.getY(), this);
	}

	private void moveY(Food closest) {
		this.board.removeProtoype(this.getX(), this.getY(), this);
		if (this.getY() < closest.getY())
			this.setY(this.getY() + 1);
		else
			this.setY(this.getY() - 1);
		this.board.add(this.getX(), this.getY(), this);
	}

	private List<Food> getFood() {
		List<Food> foods = new ArrayList<Food>();
		for (Element[] ligne : this.board.getMatrix())
			for (Element e : ligne)
				for (Prototype p : e.getList())
					if (p instanceof Food && ((Food) p).getLife() != 0)
						foods.add((Food) p);
		return foods;
	}

	private List<Food> getFreshestFood(List<Food> foods) {
		List<Food> freshest = new ArrayList<Food>();
		for (Food f : foods) {
			if (freshest.isEmpty())
				freshest.add(f);
			else {
				if (f.getLife() > freshest.get(0).getLife()) {
					freshest.clear();
					freshest.add(f);
				} else if (f.getLife() == freshest.get(0).getLife())
					freshest.add(f);
			}
		}
		return freshest;
	}

	private Food getClosestFood(List<Food> freshest) {
		Food closest = null;
		if (freshest.size() == 1)
			closest = freshest.get(0);
		else {
			int move_min = Integer.MAX_VALUE;
			for (Food f : freshest) {
				int dist = Math.abs(f.getX() - this.getX()) + Math.abs(f.getY() - this.getY());
				if (dist < move_min) {
					move_min = dist;
					closest = f;
				}
			}
		}
		return closest;
	}

	public String toString() {
		String s = "";
		s+="x : " + this.coord.get(0);
		s+="y : " + this.coord.get(1);
		return s;
	}
}
