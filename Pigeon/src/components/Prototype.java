package components;

import java.awt.Image;
import java.util.Vector;

public abstract class Prototype {
	
	protected Vector coord;
	protected Image img;
	
	public int getX() {
		return (int) this.coord.get(0);
	}
	
	public int getY() {
		return (int) this.coord.get(1);
	}
	
	public void setX(int x) {
		this.coord.set(0, x);
	}
	
	public void setY(int y) {
		this.coord.set(1, y);
	}

	public Image getImage() {
		return img;
	}
}
