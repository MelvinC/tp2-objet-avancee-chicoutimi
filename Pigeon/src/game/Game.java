package game;

import java.awt.Image;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import UI.Window;
import components.Board;
import components.Element;
import components.Food;
import components.Pigeon;
import components.Prototype;

public class Game {

	public static final int NB_PIGEON = 10;

	private Board board;
	private List<Pigeon> pigeons;
	private List<Food> foods;
	private List<Thread> threads;
	
	private Image perturb;

	private Window window;

	public Game() {
		this.board = new Board();
		this.pigeons = new ArrayList<Pigeon>();
		this.foods = new ArrayList<Food>();
		this.threads = new ArrayList<Thread>();
		try {
			this.perturb = ImageIO.read(getClass().getResource("../UI/Tracteur.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void initialize() {
		// Creation of the pigeons
		for (int i = 0; i < NB_PIGEON; i++) {
			int x = (int) (Math.random() * (Board.LENGTH));
			int y = (int) (Math.random() * (Board.HEIGHT));
			Pigeon p = new Pigeon(x, y, this.board);
			this.pigeons.add(p);
			this.board.add(x, y, p);
		}
	}

	private void removeFood() {
		for (Element[] ligne : this.board.getMatrix())
			for (Element e : ligne) {
				boolean f = false;
				boolean p = false;
				Food food = null;
				for (Prototype prot : e.getList()) {
					if (prot instanceof Food && ((Food) prot).getLife() != 0) {
						f = true;
						food = (Food) prot;
					}
					if (prot instanceof Pigeon)
						p = true;
				}
				if (p && f) {
					e.getList().remove(food);
					foods.remove(food);
				}
			}
	}

	private void decrementFood() {
		for (Element[] ligne : this.board.getMatrix())
			for (Element e : ligne) {
				Food food = null;
				for (Prototype prot : e.getList()) {
					if (prot instanceof Food) {
						food = (Food) prot;
						food.decrLife();
						if (((Food) prot).getLife() < 0 && ((Food) prot).getLife() > -20) {
							e.getList().remove(food);
							foods.remove(food);
						}

					}
				}
			}
	}
	
	private void scarePigeons() {
		double prob = Math.random() * 1000;
		if (prob > 950) {
			this.window.drawPerturb(this.perturb);
			for (Element[] ligne : this.board.getMatrix())
				for (Element e : ligne) {
					Pigeon pigeon = null;
					for (Prototype prot : e.getList()) {
						if (prot instanceof Pigeon) {
							int x = (int) (Math.random() * (Board.LENGTH));
							int y = (int) (Math.random() * (Board.HEIGHT));
							prot.setX(x);
							prot.setY(y);
						}
					}
				}
		}
	}

	public Board getBoard() {
		return this.board;
	}

	public List<Pigeon> getPigeons() {
		return this.pigeons;
	}

	public List<Food> getFoods() {
		return this.foods;
	}

	public void addFood(Food f) {
		foods.add(f);
	}

	public Window getWindow() {
		return this.window;
	}

	public static void main(String[] args) {
		Game game = new Game();
		game.initialize();
		game.window = new Window(game);
		for (Pigeon p : game.pigeons)
			game.threads.add(new Thread(p));
		for (Thread t : game.threads)
			t.start();
		while (true) {
			game.window.repaint();
			
			game.removeFood();
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			game.decrementFood();
			game.scarePigeons();
		}
	}
}
